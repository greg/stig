FROM registry.access.redhat.com/ubi8:latest

USER root

# Install dependencies, download the oscap-podman script, and cleanup
RUN dnf update -y && \
    dnf install -y curl openscap openscap-scanner podman && \
    curl -L https://raw.githubusercontent.com/OpenSCAP/openscap/maint-1.3/utils/oscap-podman -o /usr/bin/oscap-podman 
